import sys
import os
import sensor.terminal.index
import _thread


class Brain:
  def __init__(self):
    pass

  def run(self):
    # load first module
    self.load('cmd')

  def load(self, module):
    t = sensor.terminal.index.Terminal()

    pipeout, pipein = os.pipe()

    _thread.start_new_thread(t.stream, (pipein, ))

    while True:
      c = os.read(pipeout, 10).decode()[:-1]

      print('c ', c)
