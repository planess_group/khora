import os
import sys


class Terminal:
  def stream(self, pipein):
    while True:
      c = self.readData()
      t = c.strip().split()

      print('-', t, len(t))

      for w in t:
        os.write(pipein, (w + '\n').encode())

  def readData(self):
    return sys.stdin.readline()
